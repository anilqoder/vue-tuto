@section('title', 'Master Dashboard')
@extends('layouts.master-layout')
@inject('getDealerDetails', 'App\Services\RoleType')
@inject('dealerSelectedUnits','App\Services\DealerChoosenUnit')
@push('mystyles')
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
    <link href="{{ url('/css/clientdashboard.css') }}" rel="stylesheet">
    <style type="text/css">
        .dataTables_scrollBody{
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }

         .dataTables_scrollBody::-webkit-scrollbar {
            width: 7px;
            /*height: 726px;*/
            border-radius: 5px;
            background-color: #f5f5f5;
        }
        .dataTables_scrollBody::-webkit-scrollbar-track {
            /*box-shadow: inset 0 0 5px grey;*/
            border-radius: 10px;
        }
        .dataTables_scrollBody::-webkit-scrollbar-thumb {
            width: 7px;
            height: 213px;
            border-radius: 5px;
            background-color: #d4d4d4;
        }
        .dataTables_scrollBody::-webkit-scrollbar-thumb:hover {
            background: #9d9ea0;
        }
        #inventary_list_table tbody tr:last-child td{
            padding-bottom: 30px;
        }
        .dataTables_scrollBody{
            height: 610px !important;
        }
        .dataTables_wrapper.no-footer div.dataTables_scrollHead table{
            width: 874px !important;
        }
        .data-table-section table tbody td a {
            line-height: 10px;
        }
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover{
            background-color: #ffffff !important;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd{
            background-color: #ffffff !important;
        }
        table.dataTable.row-border tbody th, table.dataTable.row-border tbody td, table.dataTable.display tbody th, table.dataTable.display tbody td{
            border-top: none;
        }
        .data-table-section .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-radius: 10px !important;
        }
        .set-width {
            width: 100%;
            max-width: 183px;
        }
        .vehicle_connection_box_new{
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
            border-radius: 10px;
            background-color: #333333;
            padding: 30px 32px 0px;
            margin: 16px 46px;
            /*background-color: #ffffff;*/
        }
        .status-title{
            color: #ffffff;
            font-size: 30px;
            font-weight: 400;
        }
        .status-list ul {
            margin: 0;
            padding: 0;
            font-size: 0;
        }
        .status-list ul li {
            display: inline-block;
            width: 16%;
            padding: 28px 0px;
            margin: 0px -1%;
        }
        .circular-status{
            width: 120px;
            height: 120px;
            border-radius: 50%;
            background-color: rgba(255, 255, 255, 0.1);
            margin: 0 auto 25px;
        }
        li span{
            color: #ffffff;
        }
        .status-list li a:hover {
            text-decoration: none;
            background-color: #333 !important;
            color: black;
        }
        .circular-status img{
            height: 60px;
            top: 26%;
            left: 0px;
        }
        .status-count{
            color: #ffffff;
            font-size: 48px;
            font-weight: 400;
            line-height: 30px;
            margin-bottom: 14px;
        }
        .status-alerts{
            color: #999999;
            font-size: 18px;
            font-weight: 400;
            line-height: 30px;
            /*min-width: 147px;*/
        }
        .pad-set{
            padding: 23px 46px;
        }
        .shade-design{
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
            border-radius: 10px;
        }
    </style>
@endpush
@section('content')
<div class="container-fluid container-wrapper">
    <section class="p_15">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-lg-12">
                        <span class="intro-text">Hello {{ $username->full_name }}</span>
                    </div>
                    <div class="col-lg-12 help-text-div">
                        <span class="help-text">Here’s a quick look at what’s going on at what’s happening on the Dealership lot.</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('alerts-list/1') }}" class="common_link view_all right-25 set-width">View All</a>
            </div>
        </div>
    </section>
    <section class="p_15 vehicle_connection_box vehicle_connection_box_new">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <span class="status-title">Status Bar</span>
            </div>
            <div class="col-lg-12 col-md-12 status-list">
                <ul>
                    <li>
                        <a href="{{ url('/loaner-charges') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/img/pocket-drive-billing.png') }}" height="60" alt="">
                            </div>
                            <div class="status-count">0</div>
                            <div class="status-alerts">Billing Due</div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('active/vehicle-list/0') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/img/pocket-drive-connected.png') }}" height="60" alt="">
                            </div>
                            <div class="status-count">{{ $connectedDevices }}</div>
                            <div class="status-alerts">Connected Devices</div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('alerts-list/6') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/images/disconnection-alert.svg') }}" height="60" alt="">
                            </div>
                            <div class="status-count">{{ $disconnectedVehicleCount }}</div>
                            <div class="status-alerts">Disconnected Devices</div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('alerts-list/2') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/images/battery-alert.svg') }}" height="60" alt="">
                            </div>
                            <div class="status-count">{{ $lowBatteryCount }}</div>
                            <div class="status-alerts">Battery Alerts</div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('alerts-list/3') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/images/fuel-alert.svg') }}" height="60" alt="">
                            </div>
                            <div class="status-count">{{ $lowFuelCount }}</div>
                            <div class="status-alerts">Fuel Alerts</div>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('alerts-list/4') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/images/gas-alert.svg') }}" height="60" alt="">
                            </div>
                            <div class="status-count">{{ $gpsCount }}</div>
                            <div class="status-alerts">GPS Alerts</div>
                        </a>
                    </li>


                    <li>
                        <a href="{{ url('alerts-list/4') }}">
                            <div class="circular-status">
                                <img src="{{ asset('/img/pocket-drive-mileage.png') }}" height="60" alt="">
                            </div>
                            <div class="status-count">{{ $lowMileageCount }}</div>
                            <div class="status-alerts">Mileage Alerts</div>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    <section class="p_15 top_10 mb-63 dashboard_inventary pad-set">
        <div class="row no-gutters shade-design">
            <div class="col-lg-6 inventrary_list data-table-section">
                {{-- {{dd($dashboardData['message']['vehiclesInfo'])}} --}}
                <table id="inventary_list_table" class="display datatable_common_design" style="width: 100%;">
                    <thead>
                    <tr>
                        <th width="40%">Vin#</th>
                        <th width="20%">Battery</th>
                        <th width="20%">Fuel</th>
                        <th width="20%">Mileage</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($vehiclesInfo != null)
                            @foreach ($vehiclesInfo as $key => $vehicleInfos)
                                <tr>
                                    <td width="28%">
                                        <div class="table-vin-number">
                                            <a href="{{url('/vechicles_profile')}}/{{ $vehicleInfos->vin_number }}">{{ $vehicleInfos->vin_number }}</a><br>
                                            @if (isset($vehicleInfos->stock_number) && $vehicleInfos->stock_number != null)
                                              <span style="color: #999999;text-transform: uppercase;line-height: 17px; " >{{ $vehicleInfos->stock_number }}</span>
                                            @else
                                             <span style="color: #999999;text-transform: uppercase;line-height: 17px; ">NA</span>
                                            @endif
                                            <br>
                                            <span style="line-height: 28px;color: rgb(0, 0, 0);">{{ $vehicleInfos->year}} {{$vehicleInfos->make}} {{$vehicleInfos->model_trim}} </span>
                                        </div>
                                    </td>
                                    <td width="25%"><div class="table-type">{{ number_format($vehicleInfos->battery_level/1000, 2) }} V</div></td>
                                            @php
                                                $total = $vehicleInfos->capacity;
                                                if(isset($total)){
                                                    $total = str_replace('L','',$total);
                                                    $data =  ($vehicleInfos->fuel_level/100)*$total;
                                                    // $diff = $total - $data;
                                                    // $cal = (($total - $data)/$total)*100;
                                                    $total = $data." L";
                                                    // dd($total);
                                                }else{
                                                    $total = '-';
                                                }
                                                // $text = $match[1][0];
                                                // $num = $match[2][0];
                                                // dd($match);
                                            @endphp
                                    <td width="25%"> <div class="table-type">
                                           @php
                                               if($dealerSelectedUnits->dealerSelectedUnits()->fuel_unit_id==1){

                                               $unit= $vehicleInfos->fuel_level*0.264172;

                                               }

                                           @endphp

                                            {{ $vehicleInfos->fuel_level }} L</div></td>
                                    <td width="22%"><div class="table-type">{{ $vehicleInfos->mileage}} KM</div></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div><!-- .vehicle_profile_part -->

            <div class="col-lg-6 vehicle_map_part " style="border: none;">
               {{--  <div id="map" style="width: 100%; height: 800px;"></div> --}}
                @component('map.MapComponent',['height'=>"800px",'locationInfo'=>$locationInfo,'datas'=>$datas])
                @endcomponent
            </div><!-- .vehicle_map_part -->
            <div class="clearfix"></div>
        </div>
    </section><!-- .dashboard-inventary -->

</div>
@endsection
@push('myScripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#inventary_list_table').DataTable({
                "sDom": 'Rfrtlip',
                "scrollY": "600px",
                "scrollCollapse": true,
                "paging": false,
                "language": { search: "<img src='/img/car.png' /> <span class='hist-title'>Inventory List<span>" },
                "bSort": false,
            });
            $('.dataTables_filter input').attr("placeholder", "Search");
        });
    </script>
@endpush
